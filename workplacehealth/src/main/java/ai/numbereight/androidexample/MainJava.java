/// Begin CodeSnippet: WorkplaceHealthExample
package ai.numbereight.androidexample;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;

import java.util.Timer;
import java.util.TimerTask;

import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.Parameters;
import ai.numbereight.sdk.types.event.Glimpse;
import ai.numbereight.sdk.types.NEActivity;
import ai.numbereight.sdk.types.NEPlace;
import ai.numbereight.sdk.types.NEWeather;

public class MainJava extends Activity {
    private static final int NOTIFICATION_ID = 0;
    private static final String CHANNEL_ID = "ai.numbereight.androidexample.channel";
    private static final String LOG_TAG = "WorkplaceHealth";
    private final NumberEight ne = new NumberEight();
    private NEPlace currentPlace;
    private NEWeather currentWeather;

    private boolean isSitting = false;
    private long activityStart = 0;
    private long inactivityStart = 0;
    private final long inactiveThreshold = 30 * 60 * 1_000L; // 30 minutes
    private final long changeThreshold = 5 * 60 * 1_000L; // 5 minutes

    private Notification activeNotification;
    private NotificationManager notificationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),  new NumberEight.OnStartListener() {
            @Override
            public void onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.");
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex);
            }
        });

        // Set up notifications
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "AndroidExample Notifications",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ne.onActivityUpdated(
			Parameters.SENSITIVITY_SMOOTH,
			new NumberEight.SubscriptionCallback<NEActivity>() {
				@Override
				public void onUpdated(@NonNull Glimpse<NEActivity> glimpse) {
					synchronized (MainJava.this) {
						NEActivity.State state = glimpse.getMostProbable().getState();

						if (state == NEActivity.State.Unknown) {
							return;
						}

						isSitting = state == NEActivity.State.Stationary
								|| state == NEActivity.State.InVehicle;
					}
				}
			}
		).onPlaceUpdated(
			new NumberEight.SubscriptionCallback<NEPlace>() {
				@Override
				public void onUpdated(@NonNull Glimpse<NEPlace> glimpse) {
					synchronized (MainJava.this) {
						currentPlace = glimpse.getMostProbable();
					}
				}
            }
		).onWeatherUpdated(
			new NumberEight.SubscriptionCallback<NEWeather>() {
				@Override
				public void onUpdated(@NonNull Glimpse<NEWeather> glimpse) {
					synchronized (MainJava.this) {
						currentWeather = glimpse.getMostProbable();
					}
				}
	        }
		);

        // Run process() on a timer loop every minute
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                process();
            }
        }, 0, 60 * 1_000L);
    }

    @Override
    public void onPause() {
        super.onPause();
        ne.unsubscribeFromAll();
    }

    private synchronized void process() {
        long now = System.currentTimeMillis();

        if (isSitting && inactivityStart == 0) {
            // Mark the start of inactivity
            inactivityStart = now;
            Log.i("AndroidExample", "Now inactive");
        } else if (!isSitting && activityStart == 0) {
            // Mark the start of activity
            activityStart = now;
            Log.i("AndroidExample", "Now active");
        }

        long timeActive = now - activityStart;
        long timeInactive = now - inactivityStart;

        if (isSitting && timeInactive > changeThreshold) {
            // Mark the end of activity if inactive for longer than the change threshold
            activityStart = 0;
        } else if (!isSitting && timeActive > changeThreshold) {
            // Mark the end of inactivity if active for longer than the change threshold
            inactivityStart = 0;
            // Reset the notification
            if (activeNotification != null) {
                notificationManager.cancel(NOTIFICATION_ID);
                activeNotification = null;
            }
        }

        // Check if the user has been inactive for too long and they
        // are at work.
        if (isSitting &&
                timeInactive > inactiveThreshold
                && currentPlace != null
                && currentPlace.getContext().getWork() ==
                    NEPlace.Context.Knowledge.AtPlaceContext
                && activeNotification == null) {

            // Generate a suggestion based on weather
            String text = "You've been sat at your desk for a while now - ";
            if (currentWeather != null) {
                if (currentWeather.getConditions() == NEWeather.Conditions.Sunny
                        && currentWeather.getTemperature().ordinal() >=
                        NEWeather.Temperature.Warm.ordinal()) {
                    text += "it's nice outside, how about a walk?";
                } else {
                    text += "maybe walk around the office for a while?";
                }
            } else {
                text += "perhaps walk around for a bit?";
            }

            // Display the text as a notification
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                activeNotification = new Notification.Builder(this, CHANNEL_ID)
                        .setContentText(text)
                        .build();
            } else {
                //noinspection deprecation
                activeNotification = new Notification.Builder(this)
                        .setContentText(text)
                        .getNotification();
            }

            notificationManager.notify(NOTIFICATION_ID, activeNotification);
        }
    }
}
/// End CodeSnippet: WorkplaceHealthExample
