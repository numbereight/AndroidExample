/// Begin CodeSnippet: ContextMusicExample
package ai.numbereight.androidexample

import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import java.util.*

import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.Parameters
import ai.numbereight.sdk.types.NESituation

class MainKotlin : Activity() {

    private val ne = NumberEight()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(), object: NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        Log.i(LOG_TAG, "Waiting for situation updates...")
        ne.onSituationUpdated(Parameters.CHANGES_MOST_PROBABLE_ONLY) { glimpse ->
            val major = glimpse.mostProbable.major
            val playlist = getPlaylist(tagLookup[major].orEmpty())

            val text = "New playlist: " + TextUtils.join(",", playlist)
            Log.i(LOG_TAG, text)
        }
    }

    override fun onPause() {
        super.onPause()
        ne.unsubscribeFromAll()
    }

    private fun getPlaylist(tags: List<String>): List<String> {
        return Collections.nCopies(10, "Rick Astley - Never Gonna Give You Up")
    }

    companion object {
        private const val LOG_TAG = "ContextMusic"

        private val tagLookup = mapOf(
                NESituation.Major.Travelling to listOf("steampunk", "dj",
                        "driving", "progressive", "feel good", "groovy", "roadtrip", "super", "funky",
                        "psycheledic"),
                NESituation.Major.Housework to listOf("energetic", "catchy",
                        "happy", "sweet", "guilty pleasure", "smile", "sing along", "positive", "fun",
                        "quirky", "ambient"),
                NESituation.Major.Leisure to listOf("relaxing",
                        "atmospheric", "easy listening", "soothing", "lounge", "chill", "summer",
                        "uplifting", "chillout", "beautiful", "beach", "sunny"),
                NESituation.Major.MorningRituals to listOf("summer",
                        "upbeat", "feel good", "awesome", "chill", "morning", "happy", "amazing",
                        "acoustic", "fun"),
                NESituation.Major.Shopping to listOf("steampunk", "dj",
                        "driving", "progressive", "feel good", "groovy", "roadtrip", "super", "funky",
                        "psycheledic"),
                NESituation.Major.Sleeping to listOf("relaxing", "calm",
                        "mellow", "dreamy", "sad", "beautiful", "ballad", "meditative", "peaceful",
                        "soothing", "relaxation", "instrumental"),
                NESituation.Major.Social to listOf("dance", "party", "pop",
                        "upbeat", "club", "schlager", "house", "summer", "remix", "electro"),
                NESituation.Major.Working to listOf("chill", "classical",
                        "piano", "instrumental", "new age", "ambient", "beautiful", "mellow", "relax",
                        "soundtrack", "composer", "studying", "working"),
                NESituation.Major.WorkingOut to listOf("dance",
                        "electronic", "gym", "house", "fun", "club", "party", "mix", "mashups",
                        "workout", "motivation", "running", "cardio")
        )
    }
}
/// End CodeSnippet: ContextMusicExample
