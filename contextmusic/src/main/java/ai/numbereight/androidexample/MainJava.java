/// Begin CodeSnippet: ContextMusicExample
package ai.numbereight.androidexample;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.Parameters;
import ai.numbereight.sdk.types.event.Glimpse;
import ai.numbereight.sdk.types.NESituation;

public class MainJava extends Activity {
    private static final String LOG_TAG = "ContextMusic";

    private static final TreeMap<NESituation.Major, List<String>> tagLookup;

    private final NumberEight ne = new NumberEight();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(), new NumberEight.OnStartListener() {
            @Override
            public void onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.");
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        
        Log.i(LOG_TAG, "Waiting for situation updates...");
        ne.onSituationUpdated(
                Parameters.CHANGES_MOST_PROBABLE_ONLY,
                new NumberEight.SubscriptionCallback<NESituation>() {
                    @Override
                    public void onUpdated(Glimpse<NESituation> glimpse) {
                        NESituation.Major major;
                        major = glimpse.getMostProbable().getMajor();

                        List<String> tags = tagLookup.get(major);
                        if (tags != null) {
                            List<String> playlist = getPlaylist(tags);

                            String text = "New playlist: " + TextUtils.join(",", playlist);
                            Log.i(LOG_TAG, text);
                        }
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        ne.unsubscribeFromAll();
    }

    private List<String> getPlaylist(List<String> tags) {
        return Collections.nCopies(10, "Rick Astley - Never Gonna Give You Up");
    }

    static {
        tagLookup = new TreeMap<>();
        tagLookup.put(NESituation.Major.Travelling, Arrays.asList("steampunk", "dj",
                "driving", "progressive", "feel good", "groovy", "roadtrip", "super", "funky",
                "psycheledic"));
        tagLookup.put(NESituation.Major.Housework, Arrays.asList("energetic", "catchy",
                "happy", "sweet", "guilty pleasure", "smile", "sing along", "positive", "fun",
                "quirky", "ambient"));
        tagLookup.put(NESituation.Major.Leisure, Arrays.asList("relaxing",
                "atmospheric", "easy listening", "soothing", "lounge", "chill", "summer",
                "uplifting", "chillout", "beautiful", "beach", "sunny"));
        tagLookup.put(NESituation.Major.MorningRituals, Arrays.asList("summer",
                "upbeat", "feel good", "awesome", "chill", "morning", "happy", "amazing",
                "acoustic", "fun"));
        tagLookup.put(NESituation.Major.Shopping, Arrays.asList("steampunk", "dj",
                "driving", "progressive", "feel good", "groovy", "roadtrip", "super", "funky",
                "psycheledic"));
        tagLookup.put(NESituation.Major.Sleeping, Arrays.asList("relaxing", "calm",
                "mellow", "dreamy", "sad", "beautiful", "ballad", "meditative", "peaceful",
                "soothing", "relaxation", "instrumental"));
        tagLookup.put(NESituation.Major.Social, Arrays.asList("dance", "party", "pop",
                "upbeat", "club", "schlager", "house", "summer", "remix", "electro"));
        tagLookup.put(NESituation.Major.Working, Arrays.asList("chill", "classical",
                "piano", "instrumental", "new age", "ambient", "beautiful", "mellow", "relax",
                "soundtrack", "composer", "studying", "working"));
        tagLookup.put(NESituation.Major.WorkingOut, Arrays.asList("dance",
                "electronic", "gym", "house", "fun", "club", "party", "mix", "mashups",
                "workout", "motivation", "running", "cardio"));
    }
}
/// End CodeSnippet: ContextMusicExample
