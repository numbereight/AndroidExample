# NumberEight Android Examples

In here you will find several sample apps to showcase how to use the [NumberEight SDK](https://docs.numbereight.ai).

## GUI-based examples

* `mainexample` is a small UI-based app which shows current Activity.
* `insightsexample` is a simple UI app which records context with NumberEight Insights.
* `joggingexample` displays jog-o-meter that shows whether the user is currently jogging or not.

## Console-based examples

* `devicepositiondetection` prints the current device position to Logcat.
* `situationandplace` prints the current situation and place together to Logcat.
* `contextmusic` prints music playlists to Logcat depending on the user's context, although the only playlists you'll be getting is Rick Astley...
* `workplacehealth` displays a notification if you've been sitting at work for more than half an hour without a rest break.

### Adding your developer API key

You will need to [**generate a developer key**](https://portal.eu.numbereight.ai/keys) to authenticate with.

The recommended way to run these examples with a real API key is to create a new file in the main project directory called `keystore.properties`.

Inside the file, paste the following:

    # AndroidExample/keystore.properties
    # DO NOT ADD THIS TO GIT
    NESDK_KEY=insert_your_developer_key_here

After syncing Gradle, your API key should now be available in BuildConfig.

Alternatively, replace all mentions of `BuildConfig.REPLACE_WITH_DEVELOPER_KEY` with your key. This is generally not a good practice, as you could accidentally end up adding your API key to a public git repository!

### Documentation

See the [official documentation](http://docs.numbereight.ai/) for guides on setting up your own project and a list of what can be detected.

### Maintainers

* [Chris Watts](chris@numbereight.ai)

