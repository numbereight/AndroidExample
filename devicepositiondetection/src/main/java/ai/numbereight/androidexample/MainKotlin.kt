/// Begin CodeSnippet: DevicePositionDetectionExample
package ai.numbereight.androidexample

import android.app.Activity
import android.os.Bundle
import android.util.Log

import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.ConsentOptions

class MainKotlin : Activity() {
    companion object {
        private const val LOG_TAG = "DevicePositionDetection"
    }

    private val ne = NumberEight()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(), object: NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        ne.onDevicePositionUpdated { glimpse ->
            val position = glimpse.mostProbable
            Log.i("AndroidExample", position.toString())
        }
    }

    override fun onPause() {
        super.onPause()
        ne.unsubscribeFromAll()
    }
}
/// End CodeSnippet: DevicePositionDetectionExample
