/// Begin CodeSnippet: DevicePositionDetectionExample
package ai.numbereight.androidexample;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;

import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.types.NEDevicePosition;
import ai.numbereight.sdk.types.event.Glimpse;

public class MainJava extends Activity {
    private static final String LOG_TAG = "DevicePositionDetection";
    private final NumberEight ne = new NumberEight();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),         new NumberEight.OnStartListener() {
            @Override
            public void onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.");
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        ne.onDevicePositionUpdated(
                new NumberEight.SubscriptionCallback<NEDevicePosition>() {
                    @Override
                    public void onUpdated(@NonNull Glimpse<NEDevicePosition> glimpse) {
                        NEDevicePosition position = glimpse.getMostProbable();
                        Log.i(LOG_TAG, position.toString());
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        ne.unsubscribeFromAll();
    }
}
/// End CodeSnippet: DevicePositionDetectionExample
