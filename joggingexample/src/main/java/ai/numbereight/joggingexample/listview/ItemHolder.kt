package ai.numbereight.joggingexample.listview

import androidx.recyclerview.widget.RecyclerView
import android.view.View

abstract class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun update(item: Item)
}