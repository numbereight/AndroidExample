package ai.numbereight.joggingexample.listview

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ai.numbereight.joggingexample.R

class ItemAdapter(context: Context) : RecyclerView.Adapter<ItemHolder>() {
    private val inflater = LayoutInflater.from(context)
    private val items = mutableListOf<Item>()
    private val mappings = mutableMapOf<String, Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return when (viewType) {
            EntryItem.Holder.VIEW_TYPE -> {
                val layout = inflater.inflate(R.layout.history_entry, parent, false)
                EntryItem.Holder(layout)
            }
            else -> throw IllegalArgumentException("$viewType is not a valid item type")
        }
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item = items[position]
        holder.update(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].viewType
    }

    fun add(value: Item, key: String? = null) {
        val position = this.items.size
        this.items.add(position, value)

        if (key != null) {
            this.mappings[key] = position
        }

        notifyItemChanged(position)
    }

    fun addAll(items: Collection<Item>) {
        val position = this.items.size
        this.items.addAll(position, items)

        notifyItemRangeChanged(position, this.items.size)
    }

    fun addAll(items: Map<String, Item>) {
        val previousSize = this.items.size

        items.forEach {
            val position = this.items.size
            this.items.add(position, it.value)
            this.mappings[it.key] = position
        }

        notifyItemRangeChanged(previousSize, items.size)
    }

    operator fun get(key: String): Item? {
        return this.mappings[key]?.let {
            this.items[it]
        }
    }

    operator fun set(key: String, value: Item) {
        this.mappings[key]?.let {
            this.items[it] = value
            notifyItemChanged(it)
        } ?: add(value, key)
    }

    fun clear() {
        this.items.clear()
        this.mappings.clear()

        notifyDataSetChanged()
    }
}