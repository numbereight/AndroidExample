package ai.numbereight.joggingexample.listview

import android.view.View
import android.widget.TextView
import ai.numbereight.joggingexample.JoggingSession
import ai.numbereight.joggingexample.R
import java.text.SimpleDateFormat
import java.util.*

class EntryItem(val start: Date?, val end: Date?) : Item {
    override val viewType = Holder.VIEW_TYPE

    constructor(session: JoggingSession) : this(session.start, session.end)

    class Holder(itemView: View) : ItemHolder(itemView) {
        companion object {
            const val VIEW_TYPE = 0
        }

        private val start: TextView = itemView.findViewById(R.id.history_entry_start)
        private val end: TextView = itemView.findViewById(R.id.history_entry_end)

        override fun update(item: Item) {
            val entry = item as? EntryItem
            if (entry != null) {
                val formatter = SimpleDateFormat("dd/MM/yy HH:mm", Locale.UK)
                start.text = if (entry.start != null) {
                    formatter.format(entry.start)
                } else {
                    ""
                }
                end.text = if (entry.end != null) {
                    formatter.format(entry.end)
                } else {
                    ""
                }
            }
        }
    }
}