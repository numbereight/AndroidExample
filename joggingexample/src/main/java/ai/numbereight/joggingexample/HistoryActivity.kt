package ai.numbereight.joggingexample

import com.google.android.material.bottomnavigation.BottomNavigationView

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import io.realm.Realm
import io.realm.Sort
import java.util.*

import ai.numbereight.joggingexample.listview.EntryItem
import ai.numbereight.joggingexample.listview.ItemAdapter
import ai.numbereight.sdk.NumberEight

class HistoryActivity : AppCompatActivity() {
    companion object {
        const val MAX_MERGE_GAP = 10 * 60 * 1000
        const val MIN_DURATION = 5 * 60 * 1000
        const val TIME_OFFSET = 30 * 1000
    }
    private val ne = NumberEight()
    private lateinit var itemAdapter: ItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        // Start the data service if not already started
        Intent(this, DataHistoryService::class.java).also { intent ->
            intent.action = DataHistoryService.ACTION.START_FOREGROUND
            startService(intent)
        }

        // Register navigation handlers
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav)
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_main -> {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

            true
        }

        itemAdapter = ItemAdapter(this)

        findViewById<RecyclerView>(R.id.history_list)?.run {
            adapter = itemAdapter
            layoutManager = LinearLayoutManager(this@HistoryActivity)
            itemAnimator = null
            setHasFixedSize(true)
        }

        ne.onJoggingUpdated {
            runOnUiThread {
                updateHistory()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        updateHistory()
    }

    override fun onDestroy() {
        super.onDestroy()
        ne.unsubscribeFromJogging()
    }

    private fun updateHistory() {
        itemAdapter.clear()

        Realm.getDefaultInstance().use { realm ->
            var sessions = realm.copyFromRealm(
                    realm.where(JoggingSession::class.java)
                            .sort("start", Sort.ASCENDING)
                            .findAll())

            // Merge sessions
            var previous: JoggingSession? = null
            for (session in sessions) {
                if (previous != null) {
                    val lastEnd = previous.end?.time ?: previous.start.time

                    if (session.start.time - lastEnd < MAX_MERGE_GAP) {
                        // Merge the sessions
                        session.start = previous.start
                        // Invalidate the previous session by making its duration zero
                        previous.end = previous.start
                    }
                }
                previous = session
            }

            // Remove sessions that are too short
            sessions = sessions.filter { session ->
                if (session.duration != null) {
                    session.duration!! >= MIN_DURATION
                } else {
                    false
                }
            }

            // Display sessions in descending order
            itemAdapter.addAll(sessions.reversed().map {
                // Shift the start and end times backwards to take classification inertia into consideration
                it.start = Date(it.start.time - TIME_OFFSET)
                it.end = Date(it.end!!.time - TIME_OFFSET)

                EntryItem(it)
            })
        }
    }
}