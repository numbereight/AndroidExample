package ai.numbereight.joggingexample

import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import android.util.Log
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import io.realm.Realm

class JoggingExample : Application() {
    companion object {
        private const val LOG_TAG = "JoggingExample"
    }

    override fun onCreate() {
        super.onCreate()

        // Start services
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(), object: NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex)
            }
        })
        Realm.init(this)

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(NotificationManager::class.java)

            val mainChannel = NotificationChannel(
                    DataHistoryService.NOTIFICATION.CHANNEL_ID,
                    getString(R.string.main_notification_channel_name),
                    NotificationManager.IMPORTANCE_LOW)
            mainChannel.description = getString(R.string.main_notification_channel_description)
            mainChannel.setShowBadge(false)

            // Register the channels with the system; you can't change the importance
            // or other notification behaviors after this unless the app is uninstalled
            notificationManager.createNotificationChannel(mainChannel)
        }
    }
}