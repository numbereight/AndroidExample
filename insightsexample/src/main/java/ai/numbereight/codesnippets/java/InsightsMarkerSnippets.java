package ai.numbereight.codesnippets.java;

import android.util.Log;
import androidx.annotation.NonNull;

import ai.numbereight.insights.Insights;
import ai.numbereight.insights.RecordingConfig;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.Parameters;

public class InsightsMarkerSnippets {
    private static final String LOG_TAG = "InsightsExample";

public void useDeviceId(NumberEight.APIToken token) {

/// Begin CodeSnippet: InsightsSpecifyDeviceID
RecordingConfig config = new RecordingConfig();

String deviceId = "insert_custom_device_or_user_id_here_if_required";
config.setDeviceId(deviceId);

Insights.startRecording(token, config, new NumberEight.OnStartListener() {
    @Override
    public void onSuccess() {
        Log.i(LOG_TAG, "NumberEight Insights started successfully.");
    }

    @Override
    public void onFailure(@NonNull Exception ex) {
        Log.e(LOG_TAG, "NumberEight Insights failed to start.", ex);
    }
});
/// End CodeSnippet: InsightsSpecifyDeviceID

}


public void useRecordingConfig(NumberEight.APIToken token) {
/// Begin CodeSnippet: InsightsSpecifyConfig
RecordingConfig config = new RecordingConfig();

// Add the Device Movement context to the list of recorded topics
config.getTopics().add(NumberEight.kNETopicDeviceMovement);

// Put a smoothing filter on the Device Movement context
config.getFilters().put(
    NumberEight.kNETopicDeviceMovement,
    Parameters.SENSITIVITY_SMOOTHER.and(Parameters.CHANGES_ONLY).getFilter());

Insights.startRecording(token, config, new NumberEight.OnStartListener() {
    @Override
    public void onSuccess() {
        Log.i(LOG_TAG, "NumberEight Insights started successfully.");
    }

    @Override
    public void onFailure(@NonNull Exception ex) {
        Log.e(LOG_TAG, "NumberEight Insights failed to start.", ex);
    }
});
/// End CodeSnippet: InsightsSpecifyConfig
}

}
