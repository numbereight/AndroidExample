package ai.numbereight.codesnippets.kotlin

import ai.numbereight.insights.Insights
import android.app.Activity
import android.os.Bundle

/// Begin CodeSnippet: InsightsMarkerEvents
class MyMarkerActivity : Activity() {
    override fun onResume() {
        super.onResume()
        Insights.addMarker("screen_viewed")
    }

    fun purchaseMade(value: Int) {
        val params = Bundle()
        params.putInt("value", value)

        Insights.addMarker("in_app_purchase", params)
    }

    fun songPlayed(title: String, artist: String, genre: String) {
        val params = Bundle()
        params.putString("title", title)
        params.putString("artist", artist)
        params.putString("genre", genre)

        Insights.addMarker("song_played", params)
    }
}
/// End CodeSnippet: InsightsMarkerEvents
