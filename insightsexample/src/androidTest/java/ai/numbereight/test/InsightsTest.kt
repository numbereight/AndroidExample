package ai.numbereight.test

import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.insights.Insights
import ai.numbereight.insights.RecordingConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class InsightsTest {
    companion object {
        private const val LOG_TAG = "InsightsExample"
    }

    @Test
    fun testInsightsStart() {
        val neCountDownLatch = CountDownLatch(1)
        val insightsCountDownLatch = CountDownLatch(1)

        val token = NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            InstrumentationRegistry.getInstrumentation().targetContext,
            ConsentOptions.withConsentToAll(),
            object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                    neCountDownLatch.countDown()
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                    assertNull("An exception was returned by NumberEight.start()", ex)
                    neCountDownLatch.countDown()
                }
            }
        )

        val config = RecordingConfig()
        Insights.startRecording(token, config,
            object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight Insights started successfully.")
                    insightsCountDownLatch.countDown()
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight Insights failed to start.", ex)
                    assertNull("An exception was returned by Insights.startRecording()", ex)
                    insightsCountDownLatch.countDown()
                }
            }
        )

        neCountDownLatch.await(5, TimeUnit.SECONDS)
        insightsCountDownLatch.await(5, TimeUnit.SECONDS)

        if (neCountDownLatch.count != 0L) {
            assertTrue("NumberEight.start() timed out", false)
        }

        if (insightsCountDownLatch.count != 0L) {
            assertTrue("Insights.startRecording() timed out", false)
        }
    }
}