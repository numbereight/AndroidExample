package ai.numbereight.codesnippets.java;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

import ai.numbereight.audiences.Audiences;
import ai.numbereight.audiences.Liveness;
import ai.numbereight.audiences.Membership;
import ai.numbereight.codesnippets.BuildConfig;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;

import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler;

public class AudiencesSnippets extends Activity {
    private static final String LOG_TAG = "AudiencesExample";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Start the SDK
        NumberEight.APIToken token = NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),
                        new AuthorizationChallengeHandler() {
                    @Override
                    public void requestAuthorization(@NotNull Context context,
                                                     @NotNull String[] permissions,
                                                     @NotNull Resolver resolver) {
                        // See https://developer.android.com/training/permissions/requesting
                        // for more information.
                        ActivityCompat.requestPermissions(AudiencesSnippets.this,
                                permissions,
                                8); // Choose a suitable request code here.
                        // ...
                        resolver.resolve(); // Call once user has granted/denied the permissions.
                    }
                }, new NumberEight.OnStartListener() {
                    @Override
                    public void onSuccess() {
                        Log.i(LOG_TAG, "NumberEight started successfully.");
                    }

                    @Override
                    public void onFailure(@NonNull Exception ex) {
                        Log.e(LOG_TAG, "NumberEight failed to start.", ex);
                    }
                });

        // Start recording audiences
        Audiences.startRecording(token, new NumberEight.OnStartListener() {
            @Override
            public void onSuccess() {
                Log.i(LOG_TAG, "NumberEight Audiences started successfully.");
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                Log.e(LOG_TAG, "NumberEight Audiences failed to start.", ex);
            }
        });
    }

/// Begin CodeSnippet: GetAudiences
void getAudiences() {
    Set<Membership> memberships = Audiences.getCurrentMemberships();
    for (Membership membership : memberships) {
        Log.i(LOG_TAG, membership.getName());
    }
}

void getAudienceIds() {
    Set<String> idList = Audiences.getCurrentIds();
    Log.i(LOG_TAG, idList.toString()); // e.g. ["NE-1-1", "NE-6-2"]
}

void getAudiencesExtendedIds() {
    Set<String> idList = Audiences.getCurrentExtendedIds();
    Log.i(LOG_TAG, idList.toString()); // e.g. ["NE-1-1|H", "NE-100-1|L", "NE-101-2|T"]
}

void getAudiencesIabIds() {
    Set<String> idList = Audiences.getCurrentIabIds();
    Log.i(LOG_TAG, idList.toString()); // e.g. ["408", "762"]
}

void getAudiencesExtendedIabIds() {
    Set<String> idList = Audiences.getCurrentExtendedIabIds();
    Log.i(LOG_TAG, idList.toString()); // e.g. ["408|PIFI1", "762|PIFI2|PIPV2"]
}

void getLiveAudiences() {
    Set<Membership> memberships = Audiences.getCurrentMemberships();
    for (Membership membership : memberships) {
        if (membership.getLiveness() == Liveness.LIVE) {
            Log.i(LOG_TAG, membership.getName());
        }
    }
}
/// End CodeSnippet: GetAudiences

}
