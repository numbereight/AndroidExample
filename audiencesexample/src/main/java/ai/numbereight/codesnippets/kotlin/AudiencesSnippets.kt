package ai.numbereight.codesnippets.kotlin

import ai.numbereight.audiences.Audiences
import ai.numbereight.audiences.Liveness
import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler
import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler.Resolver
import android.app.Activity
import android.util.Log
import android.content.Context
import android.os.Bundle
import androidx.core.app.ActivityCompat

class AudiencesSnippets : Activity() {
    companion object {
        private const val LOG_TAG = "AudiencesExample"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Start the SDK
        val token = NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),
                object: AuthorizationChallengeHandler {
                    override fun requestAuthorization(context: Context,
                                                      permissions: Array<String>,
                                                      resolver: Resolver) {
                        // See https://developer.android.com/training/permissions/requesting
                        // for more information.
                        ActivityCompat.requestPermissions(this@AudiencesSnippets,
                                permissions,
                                8) // Choose a suitable request code here.
                        // ...
                        resolver.resolve() // Call once user has granted/denied the permissions.
                    }
                },
                object: NumberEight.OnStartListener {
                    override fun onSuccess() {
                        Log.i(LOG_TAG, "NumberEight started successfully.")
                    }

                    override fun onFailure(ex: Exception) {
                        Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                    }
                })

        // Start recording audiences
        Audiences.startRecording(token, object : NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight Audiences started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight Audiences failed to start.", ex)
            }
        })
    }

/// Begin CodeSnippet: GetAudiences
fun getAudiences() {
    val memberships = Audiences.currentMemberships
    for (membership in memberships) {
        Log.i(LOG_TAG, membership.name)
    }
}

fun getAudienceIds() {
    val idList = Audiences.currentIds
    Log.i(LOG_TAG, idList.toString()) // e.g. ["NE-1-1", "NE-6-2"]
}

fun getAudiencesExtendedIds() {
    val idList = Audiences.currentExtendedIds()
    Log.i(LOG_TAG, idList.toString()) // e.g. ["NE-1-1|H", "NE-100-1|L", "NE-101-2|T"]
}

fun getAudiencesIabIds() {
    val idList = Audiences.currentIabIds()
    Log.i(LOG_TAG, idList.toString()) // e.g. ["408", "762"]
}

fun getAudiencesExtendedIabIds() {
    val idList = Audiences.currentExtendedIabIds()
    Log.i(LOG_TAG, idList.toString()) // e.g. ["408|PIFI1", "762|PIFI2|PIPV2"]
}

fun getLiveAudiences() {
    val memberships = Audiences.currentMemberships.filter { it.liveness == Liveness.LIVE }
    for (membership in memberships) {
        Log.i(LOG_TAG, membership.name)
    }
}
/// End CodeSnippet: GetAudiences
}
