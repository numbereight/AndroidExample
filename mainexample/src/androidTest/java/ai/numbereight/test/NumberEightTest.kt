package ai.numbereight.test

import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.types.event.Event
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Test
import android.util.Log
import java.lang.reflect.Modifier
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


class NumberEightTest {
    companion object {
        private const val LOG_TAG = "MainExample"
    }
    @Test
    fun testNativeHandle() {
        // Check a random class for the existence of mNativeHandle
        val cls = ConsentOptions::class.java
        val field = cls.declaredFields.find { it.name == "mNativeHandle" }
        assertNotNull(field)
        assertTrue(Modifier.isPrivate(field!!.modifiers))
    }

    @Test
    fun testEventConfidenceRegression() {
        // Check that Event.ValuePair.getConfidence() is a public method
        val cls = Event.ValuePair::class.java
        val method = cls.declaredMethods.find { it.name == "getConfidence" }
        assertNotNull(method)
        assertTrue(Modifier.isPublic(method!!.modifiers))
    }

    @Test
    fun testStart() {
        val countDownLatch = CountDownLatch(1)

        NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            InstrumentationRegistry.getInstrumentation().targetContext,
            ConsentOptions.withConsentToAll(),
            object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                    countDownLatch.countDown()
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                    assertTrue(ex.message, false)
                    countDownLatch.countDown()
                }
            })

        countDownLatch.await(5, TimeUnit.SECONDS)

        if (countDownLatch.count != 0L) {
            assertTrue("NumberEight.start() timed out", false)
        }
    }
}