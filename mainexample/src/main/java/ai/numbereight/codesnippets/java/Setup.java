package ai.numbereight.codesnippets.java;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ai.numbereight.codesnippets.BuildConfig;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;

/// Begin CodeSnippet: NumberEightGettingStarted
public class Setup extends Activity {
    private static final String LOG_TAG = "MainExample";

    private void initNumberEight() {
        // Initialise the NumberEight SDK from your main Activity,
        // or the first Activity that starts in your app.
        NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            this,
            ConsentOptions.withConsentToAll(),
            new NumberEight.OnStartListener() {
                @Override
                public void onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.");
                }

                @Override
                public void onFailure(@NonNull Exception ex) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex);
                }
            }
        );
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNumberEight();
    }
}
/// End CodeSnippet: NumberEightGettingStarted
