package ai.numbereight.codesnippets.java;

import android.app.Activity;
import android.content.Context;

import ai.numbereight.sdk.NumberEight;

public class UserData extends Activity {
    public void getDeviceId() {
        Context context = getApplicationContext();
/// Begin CodeSnippet: GetDeviceId
NumberEight.getDeviceId(context);
/// End CodeSnippet: GetDeviceId
    }

    public void deleteUserData() {
        Context context = getApplicationContext();
/// Begin CodeSnippet: DeleteUserData
NumberEight.deleteUserData(context);
/// End CodeSnippet: DeleteUserData
    }
}
