package ai.numbereight.codesnippets.kotlin

import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.Parameters
import ai.numbereight.sdk.common.snapshots.JSONFormatter
import ai.numbereight.sdk.common.snapshots.QueryStringFormatter
import ai.numbereight.sdk.common.snapshots.Snapshotter
import ai.numbereight.sdk.types.ConsentOptionsProperty
import ai.numbereight.sdk.types.NELockStatus
import ai.numbereight.sdk.types.NESituation
import ai.numbereight.sdk.types.NEType
import android.app.Activity
import android.util.Log

class Basics : Activity() {
    companion object {
        private const val LOG_TAG = "MainExample"
    }

    fun manageConsent() {
/// Begin CodeSnippet: ManageConsent
val consent = ConsentOptions()
consent[ConsentOptionsProperty.ALLOW_PROCESSING] = true
consent[ConsentOptionsProperty.ALLOW_STORAGE] = true
consent[ConsentOptionsProperty.ALLOW_PRECISE_GEOLOCATION] = true
consent[ConsentOptionsProperty.ALLOW_USE_FOR_PERSONALISED_CONTENT] = true

NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, consent,
    object: NumberEight.OnStartListener {
        override fun onSuccess() {
            Log.i(LOG_TAG, "NumberEight started successfully.")
        }

        override fun onFailure(ex: Exception) {
            Log.e(LOG_TAG, "NumberEight failed to start.", ex)
        }
    })

// You can also update consent at any time
NumberEight.consentOptions = consent
/// End CodeSnippet: ManageConsent
    }

    fun functions() {
/// Begin CodeSnippet: OtherGlimpses
val ne = NumberEight()

// Activity
ne.onActivityUpdated { glimpse -> }
// Device Movement
ne.onDeviceMovementUpdated { glimpse -> }
// Device Position
ne.onDevicePositionUpdated { glimpse -> }
// Indoor/Outdoor
ne.onIndoorOutdoorUpdated { glimpse -> }
// Place
ne.onPlaceUpdated { glimpse -> }
// Situation
ne.onSituationUpdated { glimpse -> }
// Time
ne.onTimeUpdated { glimpse -> }
// Weather
ne.onWeatherUpdated { glimpse -> }

// All other unnamed Glimpses
ne.subscribeTo<NELockStatus>(NumberEight.kNETopicLockStatus) { glimpse -> }
ne.subscribeTo<NEType>("") { glimpse -> }
/// End CodeSnippet: OtherGlimpses
    }

    fun usingGlimpses() {
/// Begin CodeSnippet: UsingGlimpses
val ne = NumberEight()

ne.onSituationUpdated { glimpse ->
    runOnUiThread {
        val situation = glimpse.mostProbable

        if (situation.major == NESituation.Major.Working
                && situation.minor == NESituation.Minor.InAnOffice) {
            Log.d(LOG_TAG, "User is working in an office!")
        }
    }
}
/// End CodeSnippet: UsingGlimpses
    }

    fun filteringGlimpses() {
/// Begin CodeSnippet: ParameterizedGlimpses
val ne = NumberEight()

ne.onActivityUpdated(Parameters.CHANGES_MOST_PROBABLE_ONLY) { glimpse ->
    // Only called when the most probable activity has changed
}

ne.onActivityUpdated(Parameters.SENSITIVITY_SMOOTH) { glimpse ->
    // Updates are smoothed such that abrupt changes do not trigger the callback
}

ne.onActivityUpdated(
        Parameters.SENSITIVITY_LONG_TERM and Parameters.SIGNIFICANT_CHANGE) { glimpse ->
    // Only activities that have lasted for several minutes and whose
    // confidences have changed significantly from the last glimpse will
    // trigger the callback.
}
/// End CodeSnippet: ParameterizedGlimpses
    }


  fun multipleSubscriptions() {
/// Begin CodeSnippet: MultipleSubscriptions
val manager1 = NumberEight()
val manager2 = NumberEight()

manager1.onSituationUpdated {
  // Situation subscription #1
}.onSituationUpdated {
  // Situation subscription #2
}

manager2.onActivityUpdated {
  // Activity subscription #1
}.onSituationUpdated {
  // Situation subscription #3
}

manager1.unsubscribeFromAll()
manager2.unsubscribeFromSituation()
/// End CodeSnippet: MultipleSubscriptions
  }

fun snapshotter() {
/// Begin CodeSnippet: CreateDefaultSnapshotter
NumberEight.makeSnapshotter()
/// End CodeSnippet: CreateDefaultSnapshotter

/// Begin CodeSnippet: GetDefaultSnapshotterFilters
Snapshotter.defaultTopics
Snapshotter.defaultFilters
/// End CodeSnippet: GetDefaultSnapshotterFilters

/// Begin CodeSnippet: CreateCustomSnapshotter
NumberEight.makeSnapshotter(listOf(NumberEight.kNETopicActivity, NumberEight.kNETopicPlace, NumberEight.kNETopicReachability))

NumberEight.makeSnapshotter(mapOf(
    NumberEight.kNETopicActivity to Parameters.CHANGES_ONLY,
    NumberEight.kNETopicPlace to Parameters.CHANGES_ONLY,
    NumberEight.kNETopicReachability to Parameters.CHANGES_ONLY,
))
/// End CodeSnippet: CreateCustomSnapshotter

val snapshotter = NumberEight.makeSnapshotter()
/// Begin CodeSnippet: TakeSnapshot
val snapshot = snapshotter.takeSnapshot()

val customSnapshot = snapshotter.takeSnapshot { originalTopic, glimpse ->
    Pair(originalTopic, "value:${glimpse.mostProbable} confidence:${glimpse.mostProbableConfidence}")
}
/// End CodeSnippet: TakeSnapshot

/// Begin CodeSnippet: PauseResumeSnapshotter
snapshotter.pause()
snapshotter.resume()
/// End CodeSnippet: PauseResumeSnapshotter

/// Begin CodeSnippet: FormatSnapshot
Log.d(LOG_TAG, snapshot.toString())

Log.d(LOG_TAG, snapshot.toString { state ->
    /// Your own logic here...
    state.keys.toString()
})
/// End CodeSnippet: FormatSnapshot

/// Begin CodeSnippet: UsingBuiltInFormatter
Log.d(LOG_TAG, snapshot.toString(JSONFormatter()))

Log.d(LOG_TAG, snapshot.toString(QueryStringFormatter()))
/// End CodeSnippet: UsingBuiltInFormatter
  }
}
