package ai.numbereight.codesnippets.kotlin

import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import android.app.Activity
import android.os.Bundle
import android.util.Log

/// Begin CodeSnippet: NumberEightGettingStarted
class Setup : Activity() {
    companion object {
        private const val LOG_TAG = "MainExample"
    }

    private fun initNumberEight() {
        // Initialise the NumberEight SDK from your main Activity,
        // or the first Activity that starts in your app.
        NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            this,
            ConsentOptions.withConsentToAll(),
            object : NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                }
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNumberEight()
    }
}
/// End CodeSnippet: NumberEightGettingStarted
