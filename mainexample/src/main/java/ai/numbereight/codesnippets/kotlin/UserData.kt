package ai.numbereight.codesnippets.kotlin

import ai.numbereight.sdk.NumberEight
import android.app.Activity

class UserData: Activity() {
    fun getDeviceId() {
        val context = applicationContext
/// Begin CodeSnippet: GetDeviceId
NumberEight.getDeviceId(context)
/// End CodeSnippet: GetDeviceId
    }

    fun deleteUserData() {
        val context = applicationContext
/// Begin CodeSnippet: DeleteUserData
NumberEight.deleteUserData(context)
/// End CodeSnippet: DeleteUserData
    }
}