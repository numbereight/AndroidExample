/// Begin CodeSnippet: GPSLocationExampleJava
package ai.numbereight.androidexample;

import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.Parameters;
import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler;
import ai.numbereight.sdk.types.NELocation;
import ai.numbereight.sdk.types.NELocationCoordinate2D;
import ai.numbereight.sdk.types.event.Glimpse;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

class GPSLocationJava extends AppCompatActivity {
    private static final int PERMISSIONS_REQUEST = 1;
    private static final String LOG_TAG = "LocationExample";
    private final NumberEight ne = new NumberEight();

    private AuthorizationChallengeHandler.Resolver pendingAuthChallenge = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this,
                ConsentOptions.withConsentToAll(),
            new AuthorizationChallengeHandler() {
                @Override
                public void requestAuthorization(
                        @NonNull Context context,
                        @NonNull String[] permissions,
                        @NonNull Resolver resolver) {
                    pendingAuthChallenge = resolver;

                    ActivityCompat.requestPermissions(
                            GPSLocationJava.this, permissions, PERMISSIONS_REQUEST);
                }
            }, new NumberEight.OnStartListener() {
                    @Override
                    public void onSuccess() {
                        Log.i(LOG_TAG, "NumberEight started successfully.");
                    }

                    @Override
                    public void onFailure(@NonNull Exception ex) {
                        Log.e(LOG_TAG, "NumberEight failed to start.", ex);
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST && pendingAuthChallenge != null) {
            pendingAuthChallenge.resolve();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ne.subscribeTo(NumberEight.kNETopicGPSLocation, Parameters.SENSITIVITY_REAL_TIME,
                new NumberEight.SubscriptionCallback<NELocation>() {
            @Override
            public void onUpdated(@NonNull Glimpse<NELocation> glimpse) {
                NELocationCoordinate2D mostProbableLocation =
                        glimpse.getMostProbable().getCoordinate();
                Log.i(LOG_TAG, "Got Location Update: " +
                        mostProbableLocation.getLatitude() + ", " +
                        mostProbableLocation.getLongitude());
            }
        });
        ne.subscribeTo(NumberEight.kNETopicLowPowerLocation, Parameters.SENSITIVITY_REAL_TIME,
                new NumberEight.SubscriptionCallback<NELocation>() {
            @Override
            public void onUpdated(@NonNull Glimpse<NELocation> glimpse) {
                NELocationCoordinate2D mostProbableLocation =
                        glimpse.getMostProbable().getCoordinate();
                Log.i(LOG_TAG, "Got Low Power Location Update: " +
                        mostProbableLocation.getLatitude() + ", " +
                        mostProbableLocation.getLongitude());
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        ne.unsubscribeFromAll();
    }
}
/// End CodeSnippet: GPSLocationExampleJava
