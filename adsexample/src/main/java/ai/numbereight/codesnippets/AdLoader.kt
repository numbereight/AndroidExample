package ai.numbereight.codesnippets

import java.util.HashSet
import java.util.concurrent.CompletableFuture

abstract class AdLoader {
    protected class PromiseContainer<T> {
        var promise: CompletableFuture<T>? = null
    }

    protected fun interface MediatedNetworkUpdatedHandler {
        fun onMediatedNetworkUpdated(value: String)
    }

    private val updateHandlers: MutableSet<MediatedNetworkUpdatedHandler> = HashSet()
    var mediatedNetwork: String? = null
        get() = if (field != null) field else mediationOptions[0]
        set(value) {
            require(mediationOptions.contains(value)) { "Unknown mediation option: $value" }
            field = value
            onMediatedNetworkUpdated(value!!)
        }
    val mediationOptions: List<String>
        get() = listOf("Default")

    protected fun addMediatedNetworkUpdatedHandler(handler: MediatedNetworkUpdatedHandler) {
        updateHandlers.add(handler)
    }

    protected fun removeMediatedNetworkUpdatedHandler(handler: MediatedNetworkUpdatedHandler) {
        updateHandlers.remove(handler)
    }

    private fun onMediatedNetworkUpdated(value: String) {
        for (handler in updateHandlers) {
            handler.onMediatedNetworkUpdated(value)
        }
    }

    abstract fun loadBanner(): CompletableFuture<Boolean>
    abstract fun closeBanner()
    abstract fun loadInterstitial(): CompletableFuture<Boolean>
    abstract fun cancelInterstitial()
    fun closeAll() {
        closeBanner()
        cancelInterstitial()
    }

    protected fun <T> wrapPromise(
        targetPromise: PromiseContainer<T>,
        action: (CompletableFuture<T>) -> Unit
    ): CompletableFuture<T> {
        val target = targetPromise.promise
        val promise = CompletableFuture<T>()
        if (target != null && !target.isDone) {
            promise.cancel(false)
        } else {
            targetPromise.promise = promise
            action(promise)
        }
        return promise
    }
}