package ai.numbereight.codesnippets

import ai.numbereight.audiences.Audiences
import android.app.Activity
import ai.numbereight.codesnippets.kotlin.adloaders.GoogleAdManager
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.ConsentOptions
import com.google.android.gms.ads.admanager.AdManagerAdView
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import java.lang.Exception
import java.util.ArrayList
import java.util.concurrent.CancellationException
import java.util.concurrent.CompletableFuture

class AdsMenu : Activity() {
    var loaders: MutableList<AdLoader> = ArrayList<AdLoader>()
    private var loader: AdLoader? = null
    private lateinit var adNetworkSelect: Spinner
    private lateinit var mediationSelect: Spinner
    private lateinit var errorText: TextView
    private lateinit var bannerBtn: Button
    private lateinit var i10lBtn: Button

    private fun initUI() {
        adNetworkSelect = findViewById(R.id.adNetworkSelect)
        mediationSelect = findViewById(R.id.mediationSelect)
        errorText = findViewById(R.id.infoText)
        bannerBtn = findViewById(R.id.bannerBtn)
        i10lBtn = findViewById(R.id.i10lBtn)
        val clearBtn = findViewById<Button>(R.id.clearAdsBtn)
        val backBtn = findViewById<Button>(R.id.returnBtn)

        bannerBtn.setOnClickListener { loadBanner() }
        i10lBtn.setOnClickListener { loadInterstitial() }

        clearBtn.setOnClickListener { clearAll() }
        backBtn.setOnClickListener { finish() }

        adNetworkSelect.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectNetwork(id.toInt())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectNetwork(0)
            }
        }
        mediationSelect.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectMediationNetwork(id.toInt())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectMediationNetwork(0)
            }
        }
    }

    private fun initNumberEight() {
        // Initialise the NumberEight SDK from your main Activity,
        // or the first Activity that starts in your app.
        val token: NumberEight.APIToken = NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            this,
            ConsentOptions.withConsentToAll(),
            object : NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                }
            }
        )

        // Start Audiences
        Audiences.startRecording(token, object : NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight Audiences started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight Audiences failed to start.", ex)
            }
        })
    }

    private fun initAds() {
        val gamBannerView = findViewById<AdManagerAdView>(R.id.gamBannerAdView)
        loaders.add(GoogleAdManager(this, gamBannerView))
        adNetworkSelect.adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item, listOf(
                "Google Ad Manager"
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.ads_menu)
        super.onCreate(savedInstanceState)
        initUI()
        initNumberEight()
        initAds()
        selectNetwork(0)
    }

    fun selectNetwork(index: Int) {
        this.loader?.closeAll()

        val loader = loaders[index]
        mediationSelect.adapter = ArrayAdapter<Any?>(
            this,
            android.R.layout.simple_spinner_item,
            loader.mediationOptions
        )
        this.loader = loader
    }

    fun selectMediationNetwork(index: Int) {
        val loader: AdLoader? = this.loader
        loader?.mediatedNetwork = loader?.mediationOptions?.get(index)
    }

    private fun buttonHandler(btn: Button?, task: CompletableFuture<Boolean>) {
        val originalText = btn!!.text
        btn.isEnabled = false
        btn.text = LOADING_MESSAGE
        task.whenCompleteAsync { _: Boolean?, error: Throwable? ->
            if (error == null) {
                runOnUiThread { errorText.text = null }
                return@whenCompleteAsync
            }
            if (error !is CancellationException) {
                runOnUiThread {
                    btn.text = LOADING_FAILED_MESSAGE
                    errorText.text = error.message
                }
                try {
                    Thread.sleep(2000)
                } catch (ignored: InterruptedException) {
                }
            }
        }.whenComplete { _: Boolean?, _: Throwable? ->
            runOnUiThread {
                btn.isEnabled = true
                btn.text = originalText
            }
        }
    }

    private fun loadBanner() {
        val loader: AdLoader = this.loader ?: return
        buttonHandler(bannerBtn, loader.loadBanner())
    }

    private fun loadInterstitial() {
        val loader: AdLoader = this.loader ?: return
        buttonHandler(i10lBtn, loader.loadInterstitial())
    }

    private fun clearAll() {
        for (loader in loaders) {
            loader.closeAll()
        }
    }

    companion object {
        private const val LOG_TAG = "AdsExample"
        private const val LOADING_MESSAGE = "Loading..."
        private const val LOADING_FAILED_MESSAGE = "Load Failed"
    }
}