package ai.numbereight.codesnippets.java.adloaders;

import android.app.Activity;
import android.view.View;

import ai.numbereight.codesnippets.AdLoader;
import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.ads.admanager.AdManagerAdView;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import org.jetbrains.annotations.NotNull;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ai.numbereight.audiences.Audiences;
import ai.numbereight.audiences.Liveness;
import ai.numbereight.audiences.Membership;
import kotlin.Unit;

public class GoogleAdManager extends AdLoader {
    private final @NotNull
    Activity activity;

    private final String interstitialAdUnitId = "/22836750855/test-mobile-video-interstitial";

    private final @NotNull
    AdManagerAdView bannerView;

    private final PromiseContainer<Boolean> bannerPromise = new PromiseContainer<>();
    private final PromiseContainer<Boolean> interstitialPromise = new PromiseContainer<>();

    public GoogleAdManager(@NotNull Activity activity, @NotNull AdManagerAdView bannerView) {
        super();
        this.activity = activity;
        this.bannerView = bannerView;
        // Initialize the Google Mobile Ads SDK.
        MobileAds.initialize(activity, initializationStatus -> {});
        setupAdUnits();
    }

    private void setupAdUnits() {
        AdListener bannerListener = new AdListener() {
            @Override
            public void onAdLoaded() {
                CompletableFuture<Boolean> promise = bannerPromise.getPromise();
                if (promise != null && promise.complete(true)) {
                    bannerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError error) {
                CompletableFuture<Boolean> promise = bannerPromise.getPromise();
                if (promise != null) {
                    promise.completeExceptionally(new RuntimeException(error.getMessage()));
                }
            }
        };

        bannerView.setAdListener(bannerListener);
    }

    private @NotNull
    AdManagerAdRequest generateRequest() {
        /// Begin CodeSnippet: CollectMemberships
        // Collect audiences for all liveness types
        Set<Membership> memberships = Audiences.getCurrentMemberships();
        Stream<Membership> habitual = memberships.stream().filter(it -> it.getLiveness() == Liveness.HABITUAL);
        Stream<Membership> live = memberships.stream().filter(it -> it.getLiveness() == Liveness.LIVE);
        Stream<Membership> today = memberships.stream().filter(it -> it.getLiveness() == Liveness.HAPPENED_TODAY);
        Stream<Membership> thisWeek = memberships.stream().filter(it -> it.getLiveness() == Liveness.HAPPENED_THIS_WEEK);
        Stream<Membership> thisMonth = memberships.stream().filter(it -> it.getLiveness() == Liveness.HAPPENED_THIS_MONTH);
        /// End CodeSnippet: CollectMemberships

        /// Begin CodeSnippet: MakeGAMRequest
        // Create an ad request filled with NumberEight Audiences Taxonomy IDs
        // Audiences build up over time: live audiences are available after a few seconds, whereas
        // habitual ones can take several days.
        // While testing, it is recommended to continually make new ad requests to see as many
        // audiences as possible.
        AdManagerAdRequest request = new AdManagerAdRequest.Builder()
                .addCustomTargeting("ne_habitual", habitual.map(Membership::getId).collect(Collectors.toList()))
                .addCustomTargeting("ne_live", live.map(Membership::getId).collect(Collectors.toList()))
                .addCustomTargeting("ne_today", today.map(Membership::getId).collect(Collectors.toList()))
                .addCustomTargeting("ne_this_week", thisWeek.map(Membership::getId).collect(Collectors.toList()))
                .addCustomTargeting("ne_this_month", thisMonth.map(Membership::getId).collect(Collectors.toList()))
                .build();
        /// End CodeSnippet: MakeGAMRequest

        return request;
    }

    @Override
    public @NotNull CompletableFuture<Boolean> loadBanner() {
        return wrapPromise(bannerPromise, (CompletableFuture<Boolean> promise) ->
        {
            AdManagerAdRequest request = generateRequest();
            /// Begin CodeSnippet: LoadGAMBannerAd
            bannerView.loadAd(request);
            /// End CodeSnippet: LoadGAMBannerAd
            return Unit.INSTANCE;
        });
    }

    @Override
    public void closeBanner() {
        CompletableFuture<Boolean> promise = bannerPromise.getPromise();
        if (promise != null) {
            promise.cancel(true);
        }
        bannerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public @NotNull CompletableFuture<Boolean> loadInterstitial() {
        return wrapPromise(interstitialPromise, (CompletableFuture<Boolean> promise) -> {
            AdManagerAdRequest request = generateRequest();
            InterstitialAd.load(activity, interstitialAdUnitId, request,
                    new InterstitialAdLoadCallback() {
                        @Override
                        public void onAdLoaded(@NonNull InterstitialAd ad) {
                            if (promise.complete(true)) {
                                ad.show(activity);
                            }
                        }

                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError error) {
                            promise.completeExceptionally(new RuntimeException(error.getMessage()));
                        }
                    });
            return Unit.INSTANCE;
        });
    }

    @Override
    public void cancelInterstitial() {
        CompletableFuture<Boolean> promise = interstitialPromise.getPromise();
        if (promise != null) {
            promise.cancel(true);
        }
    }
}
